import express from 'express';
import livereload from 'livereload';

const app = express();
app.use('/', express.static('.'));
const server = app.listen(3000, async () => {
    livereload.createServer().watch([
        '**/*.(html|css)',
        '!.vscode',
        '!node_modules'
    ]);
    if (process.platform === 'win32') {
        const { exec } = await import('child_process')
        exec(`start http://localhost:${server.address().port}`);
    }
})
